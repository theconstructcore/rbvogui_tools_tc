#include <fstream>
#include <iostream>
#include <math.h>
#include <ros/duration.h>
#include <ros/package.h>
#include <ros/ros.h>
#include <ros/time.h>
#include <sensor_msgs/Joy.h>
#include <sensor_msgs/NavSatFix.h>
#include <utility>

#include <robot_localization/navsat_conversions.h>
#include <tf/transform_listener.h>

double latiPoint_filtered = 0, longiPoint_filtered = 0;
double utmX_filtered = 0, utmY_filtered = 0;
double collection_time;
std::string utmZone, path_local_filtered, path_abs_filtered, gps_path_ros_pkg,
    filtered_gps_topic;
bool collect_request = true;
bool continue_collection = true;
int numWaypoints = 0, numPoints;

void filtered_gps_CB(const sensor_msgs::NavSatFix gps_msg) {
  latiPoint_filtered = gps_msg.latitude;
  longiPoint_filtered = gps_msg.longitude;
}

int main(int argc, char **argv) {
  // Initialize node and time
  ros::init(argc, argv,
            "plot_gps_waypoints"); // initiate node called plot_gps_waypoints
  ros::NodeHandle n;

  // Get params

  ros::param::get("/outdoor_waypoint_nav/filtered_coordinates_file",
                  path_local_filtered);
  ros::param::get("/outdoor_waypoint_nav/gps_path_ros_pkg", gps_path_ros_pkg);

  ros::param::get("/outdoor_waypoint_nav/num_points", numPoints);
  ros::param::get("/outdoor_waypoint_nav/collection_time", collection_time);

  ros::param::get("/outdoor_waypoint_nav/filtered_gps_topic",
                  filtered_gps_topic);

  // Initialize time and set rates
  ros::Time::init();
  double waypoint_saving_rate = 1.0 / collection_time;
  ROS_INFO("Saving Waypoints Rate ==> %f", waypoint_saving_rate);
  ros::Rate rate1(waypoint_saving_rate);

  // Subscribe to topics
  // "/outdoor_waypoint_nav/gps/filtered"
  ros::Subscriber sub_gps_filtered =
      n.subscribe(filtered_gps_topic, 100, filtered_gps_CB);

  // Read file path and create/open file
  std::string path_abs_filtered =
      ros::package::getPath(gps_path_ros_pkg) + path_local_filtered;

  std::ofstream coordFile_filtered(path_abs_filtered.c_str());

  ROS_INFO("Saving filtered coordinates to: %s", path_abs_filtered.c_str());

  if (coordFile_filtered.is_open()) {
    while (numWaypoints < numPoints) {

      ROS_INFO("Waypoint Start Sleep");
      rate1.sleep();
      ROS_INFO("Waypoint End Sleep");
      ros::spinOnce();

      if (collect_request == true) {

        // write waypoint
        std::cout << std::endl;
        ROS_INFO("Collecting new WayPoint...%d", numWaypoints);
        numWaypoints++;

        // collect one filtered point
        ROS_INFO("CB latiPoint_filtered  ==> %lf", latiPoint_filtered);
        ROS_INFO("CB longiPoint_filtered  ==> %lf", longiPoint_filtered);

        RobotLocalization::NavsatConversions::LLtoUTM(
            latiPoint_filtered, longiPoint_filtered, utmY_filtered,
            utmX_filtered, utmZone);
        coordFile_filtered << std::fixed << std::setprecision(8)
                           << utmX_filtered << " " << utmY_filtered
                           << std::endl;

        ROS_INFO("CB utmX_filtered  ==> %lf", utmX_filtered);
        ROS_INFO("CB utmY_filtered  ==> %lf", utmY_filtered);

        ROS_INFO("Waypoint collected");

      } else {
      }
    }
  } else {
    ROS_ERROR("Unable to open file.");
    ROS_INFO("Exiting..");
  }

  ROS_INFO("Closed waypoint files, you have collected "
           "%d waypoints",
           numWaypoints);
  coordFile_filtered.close();
  ROS_INFO("Ending node...");
  ros::shutdown();

  return 0;
}
