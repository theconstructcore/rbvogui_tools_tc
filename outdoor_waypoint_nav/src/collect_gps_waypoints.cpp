#include <fstream>
#include <iostream>
#include <math.h>
#include <ros/duration.h>
#include <ros/package.h>
#include <ros/ros.h>
#include <ros/time.h>
#include <sensor_msgs/Joy.h>
#include <sensor_msgs/NavSatFix.h>
#include <std_msgs/Bool.h>
#include <utility>

std::string path_local_filtered, path_abs_filtered, gps_path_ros_pkg,
    filtered_gps_topic;
double lati_point = 0, longi_point = 0, lati_last = 0, longi_last = 0;
double min_coord_change = 10 * pow(10, -6);
int numWaypoints = 0, numPoints;
double collection_time;

void filtered_gps_CB(const sensor_msgs::NavSatFix gps_msg) {
  lati_point = gps_msg.latitude;
  longi_point = gps_msg.longitude;
}

int main(int argc, char **argv) {

  // Initialize node and time
  ros::init(
      argc, argv,
      "collect_gps_waypoints"); // initiate node called collect_gps_waypoints
  ros::NodeHandle n;

  ros::param::get("/outdoor_waypoint_nav/filtered_coordinates_file",
                  path_local_filtered);
  ros::param::get("/outdoor_waypoint_nav/gps_path_ros_pkg", gps_path_ros_pkg);
  ros::param::get("/outdoor_waypoint_nav/filtered_gps_topic",
                  filtered_gps_topic);
  ros::param::get("/outdoor_waypoint_nav/num_points", numPoints);
  ros::param::get("/outdoor_waypoint_nav/collection_time", collection_time);

  ros::param::get("/outdoor_waypoint_nav/collection_time", collection_time);

  ros::Rate rate1(collection_time);

  // Read file path and create/open file
  std::string path_abs_filtered =
      ros::package::getPath(gps_path_ros_pkg) + path_local_filtered;

  ros::Time::init();
  ros::Time time_last;
  ros::Time time_current;
  ros::Duration duration_min(collection_time);

  // Initiate subscribers
  ros::Subscriber sub_gps =
      n.subscribe(filtered_gps_topic, 100, filtered_gps_CB);
  ROS_INFO("Initiated collect_gps_waypoints node");

  std::ofstream coordFile(path_abs_filtered.c_str());
  ROS_INFO("Saving coordinates to: %s", path_abs_filtered.c_str());

  time_last = ros::Time::now();

  if (coordFile.is_open()) {
    while (numWaypoints < numPoints) {

      ros::spinOnce();

      ROS_INFO("Next...numWaypoints=%d, numPoints=%d", numWaypoints, numPoints);
      // Check that there was sufficient change in position between points
      // This makes the move_base navigation smoother and stops points from
      // being collected twice
      double difference_lat =
          abs((lati_point - lati_last) * pow(10, 6)) * pow(10, -6);
      double difference_long =
          abs((longi_point - longi_last) * pow(10, 6)) * pow(10, -6);

      if ((difference_lat > min_coord_change) ||
          (difference_long > min_coord_change)) {
        // write waypoint
        ROS_INFO("Start Save Waypoint...Writing Waypoint");
        std::cout << std::endl;
        numWaypoints++;
        coordFile << std::fixed << std::setprecision(8) << lati_point << " "
                  << longi_point << std::endl;
        lati_last = lati_point;
        longi_last = longi_point;
      } else { // do not write waypoint
        ROS_WARN("Waypoint not saved, you have not moved enough "
                 "min_coord_change=%fl",
                 min_coord_change);
        ROS_WARN("New Latitude: %f   Last Latitude: %f \n", lati_point,
                 lati_last);
        ROS_WARN("New Longitude: %f   Last Longitude: %f \n", longi_point,
                 longi_last);
      }

      ROS_INFO("Sleeping...");
      rate1.sleep();
      ROS_INFO("Sleeping...Done");
    }

    coordFile.close();
    ROS_INFO("End request registered.");
  } else {
    ROS_ERROR("Unable to open file.");
    ROS_INFO("Exiting..");
  }

  ROS_INFO("Closed waypoint files, you have collected "
           "%d waypoints",
           numWaypoints);
  ROS_INFO("Ending node...");

  ros::shutdown();
  return 0;
}
